# Examples

* [hello world](hello-world) - A plugin which simply sets the title of the button to hello world when it is pressed with the number of times it's been pressed since startup.
* [CPU](cpu) - A clone of elgato's [CPU sample](https://github.com/elgatosf/streamdeck-cpu) written in Go.