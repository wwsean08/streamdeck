module gitlab.com/wwsean08/streamdeck/examples/cpu

go 1.14

replace gitlab.com/wwsean08/streamdeck => ../..

require (
	github.com/StackExchange/wmi v0.0.0-20190523213315-cbe66965904d // indirect
	github.com/go-ole/go-ole v1.2.4 // indirect
	github.com/shirou/gopsutil v2.20.7+incompatible
	gitlab.com/wwsean08/streamdeck v0.0.2
	golang.org/x/sys v0.0.0-20200828194041-157a740278f4 // indirect
)