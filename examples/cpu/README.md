# CPU

This plugin is a clone of Elgato's [CPU Plugin](https://github.com/elgatosf/streamdeck-cpu) which simply displays the CPU information of your computer updated every 500 miliseconds.

This is not localized like Elgato's example, but could be localized pretty easily I imagine.

## Build

This plugin uses the [gopsutil](https://github.com/shirou/gopsutil) library to make gathering the CPU data simpler, hence the `go mod download` command.

```bash
$ go mod download
$ GOOS=windows go build
$ cp manifest.json com.example.cpu.sdPlugin
$ cp cpu.exe com.example.cpu.sdPlugin
$ zip -r com.example.cpu.streamDeckPlugin com.example.cpu.sdPlugin
```
*NOTE*: This has only been tested on windows so I have limitted the manifest and directions to windows

This will generate a plugin that can be installed via double clicking it on windows or mac.  Alternatively you could create the com.example.cpu.sdPlugin directory and move the binaries and manifest into location manually.

## Licensing

Note that while the overall SDK library is also MIT licensed, I would like to call out that the graphics (specifically defaultImage.png, defaultImage@2x.png, icon.png, and icon@2x.png) are owned by Elgato, and licensed under the MIT license as well.