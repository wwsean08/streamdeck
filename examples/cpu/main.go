package main

import (
	"fmt"
	"math"
	"os"
	"time"

	"github.com/shirou/gopsutil/cpu"
	"gitlab.com/wwsean08/streamdeck"
)

var clicks = 0
var sdClient *streamdeck.Client
var contexts []string

func main() {
	args := os.Args[1:]
	port := args[1]
	uuid := args[3]
	go runStreamDeckClient(port, uuid)
	// loop forever while stuff happens
	for {
		time.Sleep(time.Minute)
	}
}

func runStreamDeckClient(port, uuid string) {
	var err error
	sdClient, err = streamdeck.NewClient(port, uuid)
	if err != nil {
		panic(err)
	}
	err = sdClient.Init()
	if err != nil {
		panic(err)
	}
	sdClient.SetWillAppearCallback(willAppear)
	sdClient.SetWillDisappearCallback(willDisappear)
	for {
		tick()
		time.Sleep(time.Millisecond * 500)
	}
}

func willAppear(msg streamdeck.WillAppearMsg) {
	if contexts == nil {
		contexts = make([]string, 0)
	}
	contexts = append(contexts, msg.Context)
}

func willDisappear(msg streamdeck.WillDisappearMsg) {
	found := -1
	for i, context := range contexts {
		if context == msg.Context {
			found = i
			break
		}
	}

	// if found is -1 then it was never in the list to begin with so nothing to do
	if found != -1 {
		contexts = append(contexts[:found], contexts[found+1:]...)
	}
}

func tick() {
	// There's nothing to do as nothing is being displayed
	if len(contexts) == 0 {
		return
	}

	percentage, err := cpu.Percent(0, false)
	pct := percentage[0]
	pct = math.Round(pct)
	pctInt := int(pct)

	for _, context := range contexts {
		if err != nil {
			msg := streamdeck.ShowAlertMsg{Event: streamdeck.ShowAlertEvent, Context: context}
			_ = sdClient.SendMessage(msg)
			return
		}

		msg := streamdeck.SetTitleMsg{
			Context: context,
			Event:   streamdeck.SetTitleEvent,
			Payload: struct {
				Title  string "json:\"title\""
				Target string "json:\"target\""
				State  uint   "json:\"state\""
			}{
				Title:  fmt.Sprintf("%d%%", pctInt),
				Target: streamdeck.TargetBoth,
				State:  0,
			},
		}
		sdClient.SendMessage(msg)
	}
}
