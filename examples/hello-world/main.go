package main

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/wwsean08/streamdeck"
)

var clicks = 0
var sdClient *streamdeck.Client

func main() {
	args := os.Args[1:]
	port := args[1]
	uuid := args[3]
	go runStreamDeckClient(port, uuid)
	// loop forever while stuff happens
	for {
		time.Sleep(time.Minute)
	}
}

func runStreamDeckClient(port, uuid string) {
	var err error
	sdClient, err = streamdeck.NewClient(port, uuid)
	if err != nil {
		panic(err)
	}
	err = sdClient.Init()
	if err != nil {
		panic(err)
	}
	sdClient.SetOnKeyUpCallback(onKeyUpClicked)
}

func onKeyUpClicked(msg streamdeck.KeyUpMsg) {
	clicks++
	setTitle := streamdeck.SetTitleMsg{
		Context: msg.Context,
		Event:   streamdeck.SetTitleEvent,
		Payload: struct {
			Title  string `json:"title"`
			Target string `json:"target"`
			State  uint   `json:"state"`
		}{Title: fmt.Sprintf("Hello\nWorld\n%d", clicks)},
	}
	_ = sdClient.SendMessage(setTitle)
}
