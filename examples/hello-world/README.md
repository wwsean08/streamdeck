# Hello World

This plugin will simply count the number of times it's been pressed (since the Stream Deck application was launched), and display the message "Hello World #"

## Build

```bash
$ GOOS=windows go build
$ GOOS=darwin go build
$ mkdir com.example.hello.world.sdPlugin
$ cp manifest.json com.example.hello.world.sdPlugin
$ cp hello-world.exe com.example.hello.world.sdPlugin
$ cp hello-world com.example.hello.world.sdPlugin
$ zip -r com.example.hello.world.streamDeckPlugin com.example.hello.world.sdPlugin
```

This will generate a plugin that can be installed via double clicking it on windows or mac.  Alternatively you could create the com.example.hello.world.sdPlugin directory and move the binaries and manifest into location manually.