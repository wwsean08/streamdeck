package streamdeck

const (
	PlatformWindows = "windows"
	PlatformMac     = "mac"
)

type DeviceType uint

const (
	StreamDeck DeviceType = iota
	StreamDeckMini
	StreamDeckXL
	StreamDeckMobile
	CorsairGKeys
)

// Outgoing event types
const (
	RegisterEvent                = "registerPlugin"
	LogEvent                     = "logMessage"
	SetTitleEvent                = "setTitle"
	ShowAlertEvent               = "showAlert"
	ShowOkEvent                  = "showOk"
	SetImageEvent                = "setImage"
	SetSettingsEvent             = "setSettings"
	GetSettingsEvent             = "getSettings"
	SetGlobalSettingsEvent       = "setGlobalSettings"
	GetGlobalSettingsEvent       = "getGlobalSettings"
	OpenURLEvent                 = "openUrl"
	SetStateEvent                = "setState"
	SwitchToProfileEvent         = "switchToProfile"
	SendToPropertyInspectorEvent = "sendToPropertyInspector"
	SendToPluginEvent            = "sendToPlugin"
)

// Possible target types for sent messages per elgato docs
const (
	TargetSoftware = "software"
	TargetHardware = "hardware"
	TargetBoth     = "both"
)
