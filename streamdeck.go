package streamdeck

import (
	"fmt"
	"github.com/gorilla/websocket"
	"sync"
)

type Client struct {
	conn      *websocket.Conn
	port      string
	uuid      string
	writeLock *sync.Mutex

	// callbacks
	didReceiveSettingsCallback            DidReceiveSettingsCallback
	didReceiveGlobalSettingsCallback      DidReceiveGlobalSettingsCallback
	onKeyDownCallback                     OnKeyDownCallback
	onKeyUpCallback                       OnKeyUpCallback
	willAppearCallback                    WillAppearCallback
	willDisappearCallback                 WillDisappearCallback
	titleParametersDidChangeCallback      TitleParametersDidChangeCallback
	deviceDidConnectCallback              DeviceDidConnectCallback
	deviceDidDisconnectCallback           DeviceDidDisconnectCallback
	applicationDidLaunchCallback          ApplicationDidLaunchCallback
	applicationDidTerminateCallback       ApplicationDidTerminateCallback
	systemDidWakeUpCallback               SystemDidWakeUpCallback
	propertyInspectorDidAppearCallback    PropertyInspectorDidAppearCallback
	propertyInspectorDidDisappearCallback PropertyInspectorDidDisappearCallback
	sendToPluginCallback                  SendToPluginCallback
	sendToPropertyInspectorCallback       SendToPropertyInspectorCallback
	unknownCallback                       UnknownCallback
	rawCallback                           RawCallback
}

func NewClient(port, uuid string) (*Client, error) {
	client := &Client{
		port:      port,
		uuid:      uuid,
		writeLock: &sync.Mutex{},
	}
	return client, nil
}

// Init initializes a connection to the stream deck websocket and registers
func (c *Client) Init() error {
	conn, _, err := websocket.DefaultDialer.Dial(fmt.Sprintf("ws://localhost:%s", c.port), nil)
	c.conn = conn
	err = c.register()
	if err != nil {
		return err
	}
	go c.readLoop()
	return nil
}

// register registers the connection on the stream deck websocket
func (c *Client) register() error {
	msg := RegistrationMsg{
		Event: RegisterEvent,
		UUID:  c.uuid,
	}
	return c.SendMessage(msg)
}

func (c *Client) Log(msg string) {
	logMsg := LogMsg{
		Event: LogEvent,
		Payload: struct {
			Message string `json:"message"`
		}{
			Message: msg,
		},
	}
	_ = c.SendMessage(logMsg)
}

func (c *Client) SendMessage(msg interface{}) error {
	c.writeLock.Lock()
	defer c.writeLock.Unlock()
	return c.conn.WriteJSON(msg)
}
