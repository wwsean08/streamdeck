package topics

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestGetType(t *testing.T) {
	type TestSet struct {
		stringType   string
		expectedType EventType
	}

	tests := []TestSet{
		{
			stringType:   "keyDown",
			expectedType: TypeKeyDown,
		},
		{
			stringType:   "keyUp",
			expectedType: TypeKeyUp,
		},
		{
			stringType:   "didReceiveSettings",
			expectedType: TypeDidReceiveSettings,
		},
		{
			stringType:   "didReceiveGlobalSettings",
			expectedType: TypeDidReceiveGlobalSettings,
		},
		{
			stringType:   "willAppear",
			expectedType: TypeWillAppear,
		},
		{
			stringType:   "willDisappear",
			expectedType: TypeWillDisappear,
		},
		{
			stringType:   "titleParametersDidChange",
			expectedType: TypeTitleParametersDidChange,
		},
		{
			stringType:   "deviceDidConnect",
			expectedType: TypeDeviceDidConnect,
		},
		{
			stringType:   "deviceDidDisconnect",
			expectedType: TypeDeviceDidDisconnect,
		},
		{
			stringType:   "applicationDidLaunch",
			expectedType: TypeApplicationDidLaunch,
		},
		{
			stringType:   "applicationDidTerminate",
			expectedType: TypeApplicationDidTerminate,
		},
		{
			stringType:   "systemDidWakeUp",
			expectedType: TypeSystemDidWakeUp,
		},
		{
			stringType:   "propertyInspectorDidAppear",
			expectedType: TypePropertyInspectorDidAppear,
		},
		{
			stringType:   "propertyInspectorDidDisappear",
			expectedType: TypePropertyInspectorDidDisappear,
		},
		{
			stringType:   "sendToPlugin",
			expectedType: TypeSendToPlugin,
		},
		{
			stringType:   "sendToPropertyInspector",
			expectedType: TypeSendToPropertyInspector,
		},
		{
			stringType:   "FakeTypeToGetUnknown",
			expectedType: TypeUnknown,
		},
	}

	for _, test := range tests {
		require.Equal(t, test.expectedType, GetType(test.stringType))
	}
}
