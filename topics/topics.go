package topics

type EventType string

const (
	TypeKeyDown                       = EventType("keyDown")
	TypeKeyUp                         = EventType("keyUp")
	TypeDidReceiveSettings            = EventType("didReceiveSettings")
	TypeDidReceiveGlobalSettings      = EventType("didReceiveGlobalSettings")
	TypeWillAppear                    = EventType("willAppear")
	TypeWillDisappear                 = EventType("willDisappear")
	TypeTitleParametersDidChange      = EventType("titleParametersDidChange")
	TypeDeviceDidConnect              = EventType("deviceDidConnect")
	TypeDeviceDidDisconnect           = EventType("deviceDidDisconnect")
	TypeApplicationDidLaunch          = EventType("applicationDidLaunch")
	TypeApplicationDidTerminate       = EventType("applicationDidTerminate")
	TypeSystemDidWakeUp               = EventType("systemDidWakeUp")
	TypePropertyInspectorDidAppear    = EventType("propertyInspectorDidAppear")
	TypePropertyInspectorDidDisappear = EventType("propertyInspectorDidDisappear")
	TypeSendToPlugin                  = EventType("sendToPlugin")
	TypeSendToPropertyInspector       = EventType("sendToPropertyInspector")
	TypeUnknown                       = EventType("unknownEvent")
)

func GetType(event string) EventType {
	switch EventType(event) {
	case TypeKeyDown:
		return TypeKeyDown
	case TypeKeyUp:
		return TypeKeyUp
	case TypeDidReceiveSettings:
		return TypeDidReceiveSettings
	case TypeDidReceiveGlobalSettings:
		return TypeDidReceiveGlobalSettings
	case TypeWillAppear:
		return TypeWillAppear
	case TypeWillDisappear:
		return TypeWillDisappear
	case TypeTitleParametersDidChange:
		return TypeTitleParametersDidChange
	case TypeDeviceDidConnect:
		return TypeDeviceDidConnect
	case TypeDeviceDidDisconnect:
		return TypeDeviceDidDisconnect
	case TypeApplicationDidLaunch:
		return TypeApplicationDidLaunch
	case TypeApplicationDidTerminate:
		return TypeApplicationDidTerminate
	case TypeSystemDidWakeUp:
		return TypeSystemDidWakeUp
	case TypePropertyInspectorDidAppear:
		return TypePropertyInspectorDidAppear
	case TypePropertyInspectorDidDisappear:
		return TypePropertyInspectorDidDisappear
	case TypeSendToPlugin:
		return TypeSendToPlugin
	case TypeSendToPropertyInspector:
		return TypeSendToPropertyInspector
	}
	return TypeUnknown
}
