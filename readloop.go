package streamdeck

import (
	"encoding/json"
	"os"

	"github.com/gorilla/websocket"
	"gitlab.com/wwsean08/streamdeck/topics"
)

func (c *Client) readLoop() {
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			closeErr, ok := err.(*websocket.CloseError)
			if ok {
				if closeErr.Code == websocket.CloseNormalClosure || closeErr.Code == websocket.CloseGoingAway {
					os.Exit(0)
				}
			}
			panic(err.Error())
		}

		go c.handleMessage(message)
	}
}

func (c *Client) handleMessage(msg []byte) {
	if c.rawCallback != nil {
		c.rawCallback(msg)
	}

	inMsg := &IncomingMsg{}
	err := json.Unmarshal(msg, inMsg)
	if err != nil {
		c.Log(err.Error())
		return
	}

	msgType := topics.GetType(inMsg.Event)

	switch msgType {
	case topics.TypeDidReceiveSettings:
		settingsMsg := &DidReceiveSettingsMsg{}
		err = json.Unmarshal(msg, settingsMsg)
		if err != nil {
			c.Log(err.Error())
			return
		}
		if c.didReceiveSettingsCallback != nil {
			c.didReceiveSettingsCallback(*settingsMsg)
		}
	case topics.TypeDidReceiveGlobalSettings:
		settingsMsg := &DidReceiveGlobalSettingsMsg{}
		err = json.Unmarshal(msg, settingsMsg)
		if err != nil {
			c.Log(err.Error())
			return
		}
		if c.didReceiveGlobalSettingsCallback != nil {
			c.didReceiveGlobalSettingsCallback(*settingsMsg)
		}

	case topics.TypeKeyDown:
		kdMsg := &KeyDownMsg{}
		err = json.Unmarshal(msg, kdMsg)
		if err != nil {
			c.Log(err.Error())
			return
		}
		if c.onKeyDownCallback != nil {
			c.onKeyDownCallback(*kdMsg)
		}
	case topics.TypeKeyUp:
		kuMsg := &KeyUpMsg{}
		err = json.Unmarshal(msg, kuMsg)
		if err != nil {
			c.Log(err.Error())
			return
		}
		if c.onKeyUpCallback != nil {
			c.onKeyUpCallback(*kuMsg)
		}
	case topics.TypeWillAppear:
		appearMsg := &WillAppearMsg{}
		err := json.Unmarshal(msg, appearMsg)
		if err != nil {
			c.Log(err.Error())
			return
		}
		if c.willAppearCallback != nil {
			c.willAppearCallback(*appearMsg)
		}
	case topics.TypeWillDisappear:
		disappearMsg := &WillDisappearMsg{}
		err := json.Unmarshal(msg, disappearMsg)
		if err != nil {
			c.Log(err.Error())
			return
		}
		if c.willDisappearCallback != nil {
			c.willDisappearCallback(*disappearMsg)
		}
	case topics.TypeTitleParametersDidChange:
		titleChangeMsg := &TitleParametersDidChangeMsg{}
		err := json.Unmarshal(msg, titleChangeMsg)
		if err != nil {
			c.Log(err.Error())
			return
		}
		if c.titleParametersDidChangeCallback != nil {
			c.titleParametersDidChangeCallback(*titleChangeMsg)
		}
	case topics.TypeDeviceDidConnect:
		deviceConnectMsg := &DeviceDidConnectMsg{}
		err := json.Unmarshal(msg, deviceConnectMsg)
		if err != nil {
			c.Log(err.Error())
			return
		}
		if c.deviceDidConnectCallback != nil {
			c.deviceDidConnectCallback(*deviceConnectMsg)
		}
	case topics.TypeDeviceDidDisconnect:
		deviceDisconnectMsg := &DeviceDidDisconnectMsg{}
		err := json.Unmarshal(msg, deviceDisconnectMsg)
		if err != nil {
			c.Log(err.Error())
			return
		}
		if c.deviceDidDisconnectCallback != nil {
			c.deviceDidDisconnectCallback(*deviceDisconnectMsg)
		}
	case topics.TypeApplicationDidLaunch:
		appLaunchMsg := &ApplicationDidLaunchMsg{}
		err := json.Unmarshal(msg, appLaunchMsg)
		if err != nil {
			c.Log(err.Error())
			return
		}
		if c.applicationDidLaunchCallback != nil {
			c.applicationDidLaunchCallback(*appLaunchMsg)
		}
	case topics.TypeApplicationDidTerminate:
		appTerminateMsg := &ApplicationDidTerminateMsg{}
		err := json.Unmarshal(msg, appTerminateMsg)
		if err != nil {
			c.Log(err.Error())
			return
		}
		if c.applicationDidTerminateCallback != nil {
			c.applicationDidTerminateCallback(*appTerminateMsg)
		}
	case topics.TypeSystemDidWakeUp:
		wakeUpMsg := &SystemDidWakeUpMsg{}
		err := json.Unmarshal(msg, wakeUpMsg)
		if err != nil {
			c.Log(err.Error())
			return
		}
		if c.systemDidWakeUpCallback != nil {
			c.systemDidWakeUpCallback(*wakeUpMsg)
		}
	case topics.TypePropertyInspectorDidAppear:
		propInspectorMsg := &PropertyInspectorDidAppearMsg{}
		err := json.Unmarshal(msg, propInspectorMsg)
		if err != nil {
			c.Log(err.Error())
			return
		}
		if c.propertyInspectorDidAppearCallback != nil {
			c.propertyInspectorDidAppearCallback(*propInspectorMsg)
		}
	case topics.TypePropertyInspectorDidDisappear:
		propInspectorMsg := &PropertyInspectorDidDisappearMsg{}
		err := json.Unmarshal(msg, propInspectorMsg)
		if err != nil {
			c.Log(err.Error())
			return
		}
		if c.propertyInspectorDidDisappearCallback != nil {
			c.propertyInspectorDidDisappearCallback(*propInspectorMsg)
		}
	case topics.TypeSendToPlugin:
		pluginMsg := &SendToPluginMsg{}
		err := json.Unmarshal(msg, pluginMsg)
		if err != nil {
			c.Log(err.Error())
			return
		}
		if c.sendToPluginCallback != nil {
			c.sendToPluginCallback(*pluginMsg)
		}
	case topics.TypeSendToPropertyInspector:
		propInspectorMsg := &SendToPropertyInspectorMsg{}
		err := json.Unmarshal(msg, propInspectorMsg)
		if err != nil {
			c.Log(err.Error())
			return
		}
		if c.sendToPropertyInspectorCallback != nil {
			c.sendToPropertyInspectorCallback(*propInspectorMsg)
		}
	case topics.TypeUnknown:
		if c.unknownCallback != nil {
			c.unknownCallback(*inMsg)
		}
	}
}
