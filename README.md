# streamdeck
[![pipeline status](https://gitlab.com/wwsean08/streamdeck/badges/main/pipeline.svg)](https://gitlab.com/wwsean08/streamdeck/-/commits/main)

An SDK for creating an Elgato Stream Deck plugins.  This implements the Stream Deck API outlined by Elgato [here](https://developer.elgato.com/documentation/stream-deck/sdk/overview/)

## Installation
```shell
go get gitlab.com/wwsean08/streamdeck
```

## Usage
For examples on how to use this SDK see [examples](examples), if you would like other examples, feel free to either open a PR, or an issue suggesting the examples you would like to see.

## Links
* [Stream Deck Developer Documentation](https://developer.elgato.com/documentation/stream-deck/sdk/overview/)
