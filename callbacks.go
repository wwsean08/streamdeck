package streamdeck

import (
	"fmt"
)

type DidReceiveSettingsCallback func(msg DidReceiveSettingsMsg)
type DidReceiveGlobalSettingsCallback func(msg DidReceiveGlobalSettingsMsg)
type OnKeyDownCallback func(msg KeyDownMsg)
type OnKeyUpCallback func(msg KeyUpMsg)
type WillAppearCallback func(msg WillAppearMsg)
type WillDisappearCallback func(msg WillDisappearMsg)
type TitleParametersDidChangeCallback func(msg TitleParametersDidChangeMsg)
type DeviceDidConnectCallback func(msg DeviceDidConnectMsg)
type DeviceDidDisconnectCallback func(msg DeviceDidDisconnectMsg)
type ApplicationDidLaunchCallback func(msg ApplicationDidLaunchMsg)
type ApplicationDidTerminateCallback func(msg ApplicationDidTerminateMsg)
type SystemDidWakeUpCallback func(msg SystemDidWakeUpMsg)
type PropertyInspectorDidAppearCallback func(msg PropertyInspectorDidAppearMsg)
type PropertyInspectorDidDisappearCallback func(msg PropertyInspectorDidDisappearMsg)
type SendToPluginCallback func(msg SendToPluginMsg)
type SendToPropertyInspectorCallback func(msg SendToPropertyInspectorMsg)
type UnknownCallback func(msg IncomingMsg)
type RawCallback func(msg []byte)

func (c *Client) SetDidReceiveSettingsCallback(callback DidReceiveSettingsCallback) {
	c.didReceiveSettingsCallback = callback
	// Attempt to fix the heisenbug where if I don't acknowledge the callback it will be null
	// TODO: Figure out an ACTUAL fix
	_ = fmt.Sprintf("%p", c.didReceiveSettingsCallback)
}

func (c *Client) SetDidReceiveGlobalSettingsCallback(callback DidReceiveGlobalSettingsCallback) {
	c.didReceiveGlobalSettingsCallback = callback
	// Attempt to fix the heisenbug where if I don't acknowledge the callback it will be null
	// TODO: Figure out an ACTUAL fix
	_ = fmt.Sprintf("%p", c.didReceiveGlobalSettingsCallback)
}

func (c *Client) SetOnKeyDownCallback(callback OnKeyDownCallback) {
	c.onKeyDownCallback = callback
	// Attempt to fix the heisenbug where if I don't acknowledge the callback it will be null
	// TODO: Figure out an ACTUAL fix
	_ = fmt.Sprintf("%p", c.onKeyDownCallback)
}

func (c *Client) SetOnKeyUpCallback(callback OnKeyUpCallback) {
	c.onKeyUpCallback = callback
	// Attempt to fix the heisenbug where if I don't acknowledge the callback it will be null
	// TODO: Figure out an ACTUAL fix
	_ = fmt.Sprintf("%p", c.onKeyUpCallback)
}

func (c *Client) SetWillAppearCallback(callback WillAppearCallback) {
	c.willAppearCallback = callback
	// Attempt to fix the heisenbug where if I don't acknowledge the callback it will be null
	// TODO: Figure out an ACTUAL fix
	_ = fmt.Sprintf("%p", c.willAppearCallback)
}

func (c *Client) SetWillDisappearCallback(callback WillDisappearCallback) {
	c.willDisappearCallback = callback
	// Attempt to fix the heisenbug where if I don't acknowledge the callback it will be null
	// TODO: Figure out an ACTUAL fix
	_ = fmt.Sprintf("%p", c.willDisappearCallback)
}

func (c *Client) SetTitleParametersDidChangeCallback(callback TitleParametersDidChangeCallback) {
	c.titleParametersDidChangeCallback = callback
	// Attempt to fix the heisenbug where if I don't acknowledge the callback it will be null
	// TODO: Figure out an ACTUAL fix
	_ = fmt.Sprintf("%p", c.titleParametersDidChangeCallback)
}

func (c *Client) SetDeviceDidConnectCallback(callback DeviceDidConnectCallback) {
	c.deviceDidConnectCallback = callback
	// Attempt to fix the heisenbug where if I don't acknowledge the callback it will be null
	// TODO: Figure out an ACTUAL fix
	_ = fmt.Sprintf("%p", c.deviceDidConnectCallback)
}

func (c *Client) SetDeviceDidDisconnectCallback(callback DeviceDidDisconnectCallback) {
	c.deviceDidDisconnectCallback = callback
	// Attempt to fix the heisenbug where if I don't acknowledge the callback it will be null
	// TODO: Figure out an ACTUAL fix
	_ = fmt.Sprintf("%p", c.deviceDidDisconnectCallback)
}

func (c *Client) SetApplicationDidLaunchCallback(callback ApplicationDidLaunchCallback) {
	c.applicationDidLaunchCallback = callback
	// Attempt to fix the heisenbug where if I don't acknowledge the callback it will be null
	// TODO: Figure out an ACTUAL fix
	_ = fmt.Sprintf("%p", c.applicationDidLaunchCallback)
}

func (c *Client) SetApplicationDidTerminateCallback(callback ApplicationDidTerminateCallback) {
	c.applicationDidTerminateCallback = callback
	// Attempt to fix the heisenbug where if I don't acknowledge the callback it will be null
	// TODO: Figure out an ACTUAL fix
	_ = fmt.Sprintf("%p", c.applicationDidTerminateCallback)
}

func (c *Client) SetSystemDidWakeUpCallback(callback SystemDidWakeUpCallback) {
	c.systemDidWakeUpCallback = callback
	// Attempt to fix the heisenbug where if I don't acknowledge the callback it will be null
	// TODO: Figure out an ACTUAL fix
	_ = fmt.Sprintf("%p", c.systemDidWakeUpCallback)
}

func (c *Client) SetPropertyInspectorDidAppearCallback(callback PropertyInspectorDidAppearCallback) {
	c.propertyInspectorDidAppearCallback = callback
	// Attempt to fix the heisenbug where if I don't acknowledge the callback it will be null
	// TODO: Figure out an ACTUAL fix
	_ = fmt.Sprintf("%p", c.propertyInspectorDidAppearCallback)
}

func (c *Client) SetPropertyInspectorDidDisappearCallback(callback PropertyInspectorDidDisappearCallback) {
	c.propertyInspectorDidDisappearCallback = callback
	// Attempt to fix the heisenbug where if I don't acknowledge the callback it will be null
	// TODO: Figure out an ACTUAL fix
	_ = fmt.Sprintf("%p", c.propertyInspectorDidDisappearCallback)
}

func (c *Client) SetSendToPluginCallback(callback SendToPluginCallback) {
	c.sendToPluginCallback = callback
	// Attempt to fix the heisenbug where if I don't acknowledge the callback it will be null
	// TODO: Figure out an ACTUAL fix
	_ = fmt.Sprintf("%p", c.sendToPluginCallback)
}

func (c *Client) SetSendToPropertyInspectorCallback(callback SendToPropertyInspectorCallback) {
	c.sendToPropertyInspectorCallback = callback
	// Attempt to fix the heisenbug where if I don't acknowledge the callback it will be null
	// TODO: Figure out an ACTUAL fix
	_ = fmt.Sprintf("%p", c.sendToPropertyInspectorCallback)
}

func (c *Client) SetUnknownCallback(callback UnknownCallback) {
	c.unknownCallback = callback
	// Attempt to fix the heisenbug where if I don't acknowledge the callback it will be null
	// TODO: Figure out an ACTUAL fix
	_ = fmt.Sprintf("%p", c.unknownCallback)
}

func (c *Client) SetRawCallback(callback RawCallback) {
	c.rawCallback = callback
	// Attempt to fix the heisenbug where if I don't acknowledge the callback it will be null
	// TODO: Figure out an ACTUAL fix
	_ = fmt.Sprintf("%p", c.rawCallback)
}
