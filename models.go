package streamdeck

// Repeating structs
type Coordinates struct {
	Column uint `json:"column"`
	Row    uint `json:"row"`
}

// START Incoming Messages
type IncomingMsg struct {
	Event string `json:"event"`
	Data  map[string]interface{}
}

type DidReceiveSettingsMsg struct {
	Action          string `json:"action"`
	Event           string `json:"event"`
	Context         string `json:"context"`
	Device          string `json:"device"`
	IsInMultiAction bool   `json:"isInMultiAction"`
	Payload         struct {
		Settings    map[string]interface{} `json:"settings"`
		Coordinates Coordinates            `json:"coordinates"`
	}
}

type DidReceiveGlobalSettingsMsg struct {
	Event   string `json:"event"`
	Payload struct {
		Settings map[string]interface{} `json:"settings"`
	} `json:"payload"`
}

type KeyDownMsg struct {
	Action           string `json:"action"`
	Event            string `json:"event"`
	Context          string `json:"context"`
	State            int    `json:"state"`
	UserDesiredState int    `json:"userDesiredState"`
	IsInMultiAction  bool   `json:"isInMultiAction"`
	Payload          struct {
		Settings    map[string]interface{} `json:"settings"`
		Coordinates Coordinates            `json:"coordinates"`
	} `json:"payload"`
}

type KeyUpMsg struct {
	Action           string `json:"action"`
	Event            string `json:"event"`
	Context          string `json:"context"`
	State            int    `json:"state"`
	UserDesiredState int    `json:"userDesiredState"`
	IsInMultiAction  bool   `json:"isInMultiAction"`
	Payload          struct {
		Settings    map[string]interface{} `json:"settings"`
		Coordinates Coordinates            `json:"coordinates"`
	} `json:"payload"`
}

type WillAppearMsg struct {
	Action  string `json:"action"`
	Event   string `json:"event"`
	Context string `json:"context"`
	Device  string `json:"device"`
	Payload struct {
		State           int                    `json:"state"`
		IsInMultiAction bool                   `json:"isInMultiAction"`
		Settings        map[string]interface{} `json:"settings"`
		Coordinates     Coordinates            `json:"coordinates"`
	} `json:"payload"`
}

type WillDisappearMsg struct {
	Action  string `json:"action"`
	Event   string `json:"event"`
	Context string `json:"context"`
	Device  string `json:"device"`
	Payload struct {
		State           int                    `json:"state"`
		IsInMultiAction bool                   `json:"isInMultiAction"`
		Settings        map[string]interface{} `json:"settings"`
		Coordinates     Coordinates            `json:"coordinates"`
	} `json:"payload"`
}

type TitleParametersDidChangeMsg struct {
	Action  string `json:"action"`
	Event   string `json:"event"`
	Context string `json:"context"`
	Device  string `json:"device"`
	Payload struct {
		Coordinates     Coordinates `json:"coordinates"`
		Settings        interface{} `json:"settings"`
		State           int         `json:"state"`
		Title           string      `json:"title"`
		TitleParameters struct {
			FontFamily     string `json:"fontFamily"`
			FontSize       uint   `json:"fontSize"`
			FontStyle      string `json:"fontStyle"`
			FontUnderline  bool   `json:"fontUnderline"`
			ShowTitle      bool   `json:"showTitle"`
			TitleAlignment string `json:"titleAlignment"`
			TitleColor     string `json:"titleColor"`
		}
	} `json:"payload"`
}

type DeviceDidConnectMsg struct {
	Event      string `json:"event"`
	Device     string `json:"device"`
	DeviceInfo struct {
		Name string `json:"name"`
		Type uint   `json:"type"`
		Size struct {
			Columns uint `json:"columns"`
			Rows    uint `json:"rows"`
		} `json:"size"`
	} `json:"deviceInfo"`
}

type DeviceDidDisconnectMsg struct {
	Event  string `json:"event"`
	Device string `json:"device"`
}

type ApplicationDidLaunchMsg struct {
	Event   string `json:"event"`
	Payload struct {
		Application string `json:"application"`
	} `json:"payload"`
}

type ApplicationDidTerminateMsg struct {
	Event   string `json:"event"`
	Payload struct {
		Application string `json:"application"`
	} `json:"payload"`
}

type SystemDidWakeUpMsg struct {
	Event string `json:"event"`
}

type PropertyInspectorDidAppearMsg struct {
	Action  string `json:"action"`
	Event   string `json:"event"`
	Context string `json:"context"`
	Device  string `json:"device"`
}

type PropertyInspectorDidDisappearMsg struct {
	Action  string `json:"action"`
	Event   string `json:"event"`
	Context string `json:"context"`
	Device  string `json:"device"`
}

//END Incoming Messages

//START Outgoing Messages
// RegistrationMsg represents a registration event as described at
type RegistrationMsg struct {
	Event string `json:"event"`
	UUID  string `json:"uuid"`
}

type LogMsg struct {
	Event   string `json:"event"`
	Payload struct {
		Message string `json:"message"`
	} `json:"payload"`
}

type SetTitleMsg struct {
	Event   string `json:"event"`
	Context string `json:"context"`
	Payload struct {
		Title  string `json:"title"`
		Target string `json:"target"`
		State  uint   `json:"state"`
	} `json:"payload"`
}

type ShowAlertMsg struct {
	Event   string `json:"event"`
	Context string `json:"context"`
}

type ShowOkMsg struct {
	Event   string `json:"event"`
	Context string `json:"context"`
}

type SetImageMsg struct {
	Event   string `json:"event"`
	Context string `json:"context"`
	Payload struct {
		Image  string `json:"image"`
		Target string `json:"target"`
		State  uint   `json:"state"`
	}
}

type SetSettingsMsg struct {
	Event   string      `json:"event"`
	Context string      `json:"context"`
	Payload interface{} `json:"payload"`
}

type GetSettingMsg struct {
	Event   string `json:"event"`
	Context string `json:"context"`
}

type SetGlobalSettingsMsg struct {
	Event   string      `json:"event"`
	Context string      `json:"context"`
	Payload interface{} `json:"payload"`
}

type GetGlobalSettingsMsg struct {
	Event   string `json:"event"`
	Context string `json:"context"`
}

type OpenURLMsg struct {
	Event   string `json:"event"`
	Payload struct {
		URL string `json:"url"`
	} `json:"payload"`
}

type SetStateMsg struct {
	Event   string `json:"event"`
	Context string `json:"context"`
	Payload struct {
		State uint `json:"state"`
	} `json:"payload"`
}

type SwitchToProfileMsg struct {
	Event   string `json:"event"`
	Context string `json:"context"`
	Device  string `json:"device"`
	Payload struct {
		Profile string `json:"profile"`
	} `json:"payload"`
}

// END Outgoing Messages

//Both Incoming and Outgoing Messages

type SendToPluginMsg struct {
	Action  string                 `json:"action"`
	Event   string                 `json:"event"`
	Context string                 `json:"context"`
	Payload map[string]interface{} `json:"payload"`
}

type SendToPropertyInspectorMsg struct {
	Action  string                 `json:"action"`
	Event   string                 `json:"event"`
	Context string                 `json:"context"`
	Payload map[string]interface{} `json:"payload"`
}
