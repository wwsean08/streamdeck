# v0.0.3:
## Changes:
* A bunch of documentation and example code has been written

# v0.0.2:
## Changes:
* Fixes some data types based on actual usage of the library to create a plugin and release it

# v0.0.1:
Initial release